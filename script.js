window.alert("This is my first website which I created back in 2016 during my learning phase. So do expect some mistakes here and pardon me for that, I have left it intact as my initial days work. :) ");

function main() {

    //Home****************************
    $('#logo').hide().slideDown(2000);
    $('#intro').hide().fadeIn(2000);

    //Gallery*************************
    $('.items').hide().fadeIn(2500);
    //Contact*************************

    $('#h1').hide().fadeIn(2000);
    $('#contactinfo').hide().fadeIn(2500);
    $('#input').hide().slideDown(2500);

}

$(document).ready(main);